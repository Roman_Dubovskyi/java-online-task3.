package com.epam;

public class Parrot extends Bird {
    String birdName;
    public Parrot(String birdName) {
        this.birdName = birdName;
    }
    @Override
    public  void eat(){
        System.out.println("Parrot eats only Plants! It won't take your food.");
    }
}
