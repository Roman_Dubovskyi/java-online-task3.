package com.epam;

public class Fish extends Animal {
    String fishName;
    public Fish(String fishName){
        this.fishName = fishName;
    }
    @Override
    public  void eat(){
        System.out.println("This fish is eating everything from the bottom!");
    }
    @Override
    public void sleep(){
        System.out.println("This fish is sleeping under water...");
    }
    public static void swim(){
        System.out.println("This fish is swimming.");
    }
}
