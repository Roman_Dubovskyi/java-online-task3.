package com.epam;

public class Bird extends Animal {

    @Override
    public  void eat(){
        System.out.println("Bird can eat anything!");
    }
    @Override
    public void sleep(){
        System.out.println("I'm sleeping on the branch.");
    }
    public static void fly () {
        System.out.println("This bird is flying in the cage.");
    }
}
