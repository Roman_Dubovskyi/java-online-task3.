package com.epam;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        System.out.println("Hi and welcome to my ZOO!");
        System.out.println("What is your name?");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        System.out.println("It's nice to meet you "+name);
        System.out.println("We have plenty of animals in the Zoo, i can show our aquarium with dangerous sea fish, or cage with exotic birds.");
        System.out.println("So what would you like to see?");
        String choice = sc.nextLine();
        if (choice.equals("fish")){
            System.out.println("You chose to see Fish");
            Fish whiteShark = new Fish("White Shark");
            Fish snapper = new Fish("Snapper");
            Fish dolphin = new Fish("Dolphin");
            System.out.println("Most popular are these three: "+whiteShark.fishName+", "+snapper.fishName+", "+dolphin.fishName);
            System.out.println("Let's see what they're doing now! \n We will start from "+whiteShark.fishName);
            whiteShark.sleep();
            System.out.println("Move to next one, it'll be a "+snapper.fishName);
            System.out.println("As you can see it's hungry now, if you want to feed it type \"yes\"");
            String feed = sc.nextLine();
            if (feed.equals("yes"))snapper.eat();
            System.out.println("And we have one more, it's a "+dolphin.fishName);
            dolphin.swim();
            System.out.println("You see, this one if pretty active now, you're lucky!");
            System.out.println("I guess that will be it, thank you for visiting, we'll see you next time!");
        }else if(choice.equals("birds")){
            System.out.println("You chose Birds first");
            Parrot someParrot = new Parrot("Parrot");
            Eagle someEagle = new Eagle("Eagle");
            System.out.println("Unfortunately right now we have only two birds, because ewe recently moved from a different place, they are:"+someParrot.birdName+" and "+someEagle.birdName);
            System.out.println("So which one would you like to visit first?");
            String birdChoice = sc.nextLine();
            if (birdChoice.equals("Eagle")) {
                System.out.println("Eagle! Nice choice, let's see what it's doing");
                someEagle.fly();
                System.out.println("Beautiful bird, isn't it, but there is one more, follow me.");
                System.out.println("You can see our " + someParrot.birdName + " now, it's a very exotic bird.");
                System.out.println("If you want you can feed it, just type \"feed\"");
                String feedParrot = sc.nextLine();
                if (feedParrot.equals("feed")) someParrot.eat();
                System.out.println("I guess that will be it, thank you for visiting, we'll see you next time!");
            }else if (birdChoice.equals("Parrot")){
                System.out.println("Parrot! Nice choice, let's what it's doing");
                someParrot.eat();
                System.out.println("Beautiful bird, isn't it, but there is one more, follow me.\n Look here!");
                someEagle.fly();
                System.out.println("You can see our " + someEagle.birdName + " now, it's vey rare by the way");
                System.out.println("I guess that will be it, thank you for visiting, we'll see you next time!");
            }

        }else{
            System.out.println("Wrong statement, you need type \"fish\" or \"birds\", please try again");
        }

    }
}
