package com.epam.com.epamPart2;

public class Rectangle extends Shape {
    String color;
    double firstSide;
    double secondSide;
    double square;
    public Rectangle( double firstSide, double secondSide){
        this.firstSide = firstSide;
        this.secondSide = secondSide;
    }
     void setColor(String color){
        this.color = color;
     }
     String  getColor(){
         return color;
    }
     double countSqaure(){
        double square = firstSide*secondSide;
        return square;
    }

}
