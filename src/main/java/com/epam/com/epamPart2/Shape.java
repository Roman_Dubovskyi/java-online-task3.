package com.epam.com.epamPart2;

public abstract class Shape {
    abstract String getColor();
    abstract void setColor (String color);
}
