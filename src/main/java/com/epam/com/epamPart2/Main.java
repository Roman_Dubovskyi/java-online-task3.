package com.epam.com.epamPart2;

public class Main {
    public static void main(String[] args) {
        Rectangle someRectangle = new Rectangle(23.4, 24.0);
        someRectangle.setColor("Blue");
        System.out.println("Rectangle's color is: "+someRectangle.getColor());
        System.out.println("Rectangle's square is: "+someRectangle.countSqaure());
        Circle someCircle = new Circle(90);
        someCircle.setColor("Red");
        System.out.println("Circle's color is: "+someCircle.getColor());
        System.out.println("Circle's square is: "+someCircle.countSqaure());
        Triangle someTriangle = new Triangle(10,12.5,13);
        someTriangle.setColor("Yellow");
        System.out.println("Triangle's color is: "+someTriangle.getColor());
        System.out.println("Triangle's square is: "+someTriangle.countSqaure());
    }
}
