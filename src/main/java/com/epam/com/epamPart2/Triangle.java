package com.epam.com.epamPart2;

public class Triangle extends Shape {
    String color;
    double firstSide;
    double secondSide;
    double thirdSide;
    double sqaure;
    public Triangle(double firstSide, double secondSide, double thirdSide){
        this.firstSide = firstSide;
        this.secondSide = secondSide;
        this.thirdSide = thirdSide;
    }
    void setColor(String color){
        this.color = color;
    }
    String getColor(){
        return color;
    }
    double countSqaure(){
        double p = (firstSide+secondSide+thirdSide)/2;
        sqaure = Math.sqrt(p*(p-firstSide)*(p-secondSide)*(p-thirdSide));
        return sqaure;
    }
}
