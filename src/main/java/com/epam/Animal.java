package com.epam;

 abstract class Animal {
    abstract void sleep();
    abstract void eat();
}
