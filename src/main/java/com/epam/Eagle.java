package com.epam;

public class Eagle extends Bird{
    String birdName;
    public Eagle(String birdName) {
        this.birdName = birdName;
    }
    @Override
    public void eat(){
        System.out.println("Eagle and eats only meat!");
    }

}
